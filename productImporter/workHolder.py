# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from datetime import datetime
from .utilityControl import loggerHandling, handle_uploaded_file, sendActiveUserNotificationProductNew, sendActiveUserNotificationProductUpdate, initiatedWebHookFromHere
from django.http import JsonResponse
from .dataClasses import loginHolder, sessionHolder, productHolder, productStatusHandler, pageControl, searchControl
from .dataViewHolder import isValidLoginRequest, isAllowedToPerformTask, validSessionInfo, isNewProductForUse, isAllowedToPerformChangeOnProduct, isAllowedToPerformChangeOnProductDeletePerma
from .dataContol import getLoginSessionInformation, isAllowedSkuUpdate, generateProductNotDeletedPageView, generateProductDeletedPageView, getSearchAndReturn, filetingInProductAndReturnList
from .dataMaipulator import insertSessionInfo, performLogoutOption, insertNewProductInformation, performStatusChangeOnProduct, performDeletionOnProduct, updateAllProductInfo, performDeletionOnProductPermanent, performAllSoftDeletion, performAllHardDeletion
logger = loggerHandling('log/workHolder.log')

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
####################DATA WORK HOLDER LAYER####################
##############################################################
##############################################################
##############################################################

def loginUser(request):
    try:
        rString = json.loads(request.body)
        loginRequest = loginHolder(rString['userName'], rString['password'], rString['deviceIdentifier'], 0)
        if isValidLoginRequest(loginRequest):
            sessionInfo = getLoginSessionInformation(loginRequest)
            if sessionInfo == 0:
                response = JsonResponse({"Data":"Not able to process information, please try again later"})
                response.status_code = 409
                return response
            else:
                if insertSessionInfo(sessionInfo):
                    response = JsonResponse({"Data":"Login successful","sessionInfo":{"basicAuthenticate":sessionInfo.sessionKey, "userId":sessionInfo.userId}})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Not able to create session please try again later"})
                    response.status_code = 409
                    return response
        else:
            response = JsonResponse({"Data":"Invalid username or password passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def logOutUser(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                if performLogoutOption(sessionInformation):
                    response = JsonResponse({"Data":"User logged out successfully"})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Issue while logging out user, please try after sometime"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def creatingNewProduct(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                productRequest = productHolder(rString['name'], rString['sku'], rString['desc'], 1, 0, 0)
                if isNewProductForUse(productRequest.sku):
                    if insertNewProductInformation(productRequest):
                        response = JsonResponse({"Data":"New product created successfully"})
                        response.status_code = 200
                        #Integrate webhook for creation here
                        #sendActiveUserNotificationProductNew.delay(1)
                        initiatedWebHookFromHere.delay(basicKey, sessionInformation.userId, sessionInformation.deviceIdentifier, 1, 0)
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new product, please try again later"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"A product already exists with the provided SKU"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def changingProductStatus(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                productStatusManager = productStatusHandler(rString['productId'], rString['newStatus'])
                if isAllowedToPerformChangeOnProduct(productStatusManager.productId):
                    if performStatusChangeOnProduct(productStatusManager):
                        response = JsonResponse({"Data":"Status of product has been updated"})
                        response.status_code = 200
                        #sendActiveUserNotificationProductUpdate.delay(1)
                        initiatedWebHookFromHere.delay(sessionInformation, 0, 1)
                        return response
                    else:
                        response = JsonResponse({"Data":"Not able to process product status change"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid product selected for updation"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def deletingExistingProduct(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                productStatusManager = productStatusHandler(rString['productId'], 1)
                if isAllowedToPerformChangeOnProduct(productStatusManager.productId):
                    if performDeletionOnProduct(productStatusManager):
                        response = JsonResponse({"Data":"Product Deleted successfully"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Not able to process product deletion"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid product selected for updation"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def editingProductInfo(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                productEditRequest = productHolder(rString['name'], rString['sku'], rString['desc'], 1, 0, rString['productId'])
                if isAllowedToPerformChangeOnProduct(productEditRequest.productId):
                    if isAllowedSkuUpdate(productEditRequest):
                        if updateAllProductInfo(productEditRequest):
                            response = JsonResponse({"Data":"Provided information updated for product"})
                            response.status_code = 200
                            #sendActiveUserNotificationProductUpdate.delay(1)
                            initiatedWebHookFromHere.delay(sessionInformation, 0, 1)
                            return response
                        else:
                            response = JsonResponse({"Data":"An issue encountered while updating the information, please try again later"})
                            response.status_code = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"Provided SKU is already associated with a different product"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid product selected for updation"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def deletingExistingProductPermanently(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                productStatusManager = productStatusHandler(rString['productId'], 1)
                if isAllowedToPerformChangeOnProductDeletePerma(productStatusManager.productId):
                    if performDeletionOnProductPermanent(productStatusManager):
                        response = JsonResponse({"Data":"Product Deleted permanently"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Not able to process product deletion"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid product selected for updation"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def checkingIfUniqueSku(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                sku = rString['sku']
                if isNewProductForUse(sku):
                    response = JsonResponse({"Data":"Provide SKU is available for use"})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Product with following SKU already exists"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewingAllProductList(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                pageControlHolder = pageControl(rString['pageCount'], rString['pageSize'])
                if pageControlHolder.pageCount>0:
                    if pageControlHolder.pageSize>0 and pageControlHolder.pageSize<501:
                        response = generateProductNotDeletedPageView(pageControlHolder)
                        return response
                    else:
                        response = JsonResponse({"Data":"Page size should be between 1-500"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Page number should be greater than 0"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewingAllDelProductList(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                pageControlHolder = pageControl(rString['pageCount'], rString['pageSize'])
                if pageControlHolder.pageCount>0:
                    if pageControlHolder.pageSize>0 and pageControlHolder.pageSize<501:
                        response = generateProductDeletedPageView(pageControlHolder)
                        return response
                    else:
                        response = JsonResponse({"Data":"Page size should be between 1-500"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Page number should be greater than 0"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def handleFileUploadRequest(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(request.POST['userId'], basicKey, request.POST['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                isFirstRequest = int(request.POST['isFirstRequest'])
                if isFirstRequest==1:
                    dateTimeNow = datetime.now()
                    fileName = 'media/'+str(sessionInformation.sessionKey)+str(dateTimeNow)+'.csv'
                    with open(fileName, 'a+') as destination:
                        destination.write(request.POST['file'])
                    if int(request.POST['isLastChunk'])==1:
                        taskId = handle_uploaded_file(fileName)
                        if taskId==0:
                            response = JsonResponse({"Data":"File lenght exceed the limit"})
                            response.status_code = 409
                            return response
                        else:
                            response = JsonResponse({"Data":"System is uploading and processing the request. Will update once done", "taskid":taskId})
                            response.status_code = 200
                            return response
                    else:
                        response = JsonResponse({"Data":"Upload process initiated","dateTime":dateTimeNow})
                        response.status_code = 200
                        return response
                elif isFirstRequest==0:
                    dateTimeRequest = request.POST['dateTimeRequest']
                    fileName = 'media/'+str(sessionInformation.sessionKey)+str(dateTimeRequest)+'.csv'
                    with open(fileName, 'a+') as destination:
                        destination.write(request.POST['file'])
                    if int(request.POST['isLastChunk'])==1:
                        taskId = handle_uploaded_file(fileName)
                        if taskId==0:
                            response = JsonResponse({"Data":"File lenght exceed the limit"})
                            response.status_code = 409
                            return response
                        else:
                            response = JsonResponse({"Data":"System is uploading and processing the request. Will update once done", "taskid":taskId})
                            response.status_code = 200
                            return response
                    else:
                        response = JsonResponse({"Data":"Upload process under way","dateTime":dateTimeRequest})
                        response.status_code = 200
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid upload sequence"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def searchInProducts(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                pageControlHolder = pageControl(rString['pageCount'], rString['pageSize'])
                if pageControlHolder.pageCount>0:
                    if pageControlHolder.pageSize>0 and pageControlHolder.pageSize<501:
                        searchControlHolder = searchControl(rString['searchKey'], rString['searchIn'])
                        response = getSearchAndReturn(searchControlHolder, pageControlHolder)
                        return response
                    else:
                        response = JsonResponse({"Data":"Page size should be between 1-500"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Page number should be greater than 0"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def filteringInProduct(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                pageControlHolder = pageControl(rString['pageCount'], rString['pageSize'])
                if pageControlHolder.pageCount>0:
                    if pageControlHolder.pageSize>0 and pageControlHolder.pageSize<501:
                        activeStatus = rString['isActive']
                        response = filetingInProductAndReturnList(activeStatus, pageControlHolder)
                        return response
                    else:
                        response = JsonResponse({"Data":"Page size should be between 1-500"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Page number should be greater than 0"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def deletingProductsAll(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                deleteType = rString['deleteType']
                if deleteType=="SOFT":
                    if performAllSoftDeletion():
                        response = JsonResponse({"Data":"All products softly delted"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while performing soft delition"})
                        response.status_code = 409
                        return response
                elif deleteType=="HARD":
                    if performAllHardDeletion():
                        response = JsonResponse({"Data":"All products hardly delted"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while performing soft delition"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid deletion type selected"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def workingOnWebhookRequest(request):
    try:
        rString = json.loads(request.body)
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionInformation = sessionHolder(rString['userId'], basicKey, rString['deviceIdentifier'], None, 1)
        if validSessionInfo(sessionInformation)!=0:
            if isAllowedToPerformTask(sessionInformation):
                totalNewCreated = rString['newCreated']
                totalUpdated = rString['totalUpdated']
                if int(totalNewCreated>0):
                    sendActiveUserNotificationProductNew.delay(totalNewCreated)
                if int(totalUpdated>0):
                    sendActiveUserNotificationProductUpdate.delay(totalNewCreated)
                response = JsonResponse({"Data":"Webhook recieved and websocket initiated"})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({"Data":"Invalid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid security key passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
