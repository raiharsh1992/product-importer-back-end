# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from .models import loginMaster, sessionInfo, productMaster


##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA VIEWING HOLDER LAYER###################
##############################################################
##############################################################
##############################################################

def isValidLoginRequest(loginRequest):
    loginInfo = loginMaster.objects.values_list().filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password).count()
    #for e in loginInfo:
    #    print(e[1])
    if loginInfo==1:
        return True
    else:
        return False

def getLoginObject(userIdPassed):
    loginObject = loginMaster.objects.get(userId=userIdPassed)
    return loginObject

def validSessionInfo(sessionInformation):
    sessionCheck = sessionInfo.objects.values_list().filter(sessionKey__exact=sessionInformation.sessionKey).count()
    return sessionCheck

def getUserIdFromLoginInfo(loginRequest):
    loginInfo = loginMaster.objects.values_list().filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password)
    return loginInfo[0][0]

def isAllowedToPerformTask(sessionInformation):
    sessionCheck = sessionInfo.objects.values_list().filter(sessionKey__exact=sessionInformation.sessionKey).filter(systemIdentifier__exact=sessionInformation.deviceIdentifier).filter(isActive=1).filter(userId=sessionInformation.userId).count()
    if sessionCheck==1:
        return True
    else:
        return False

def isNewProductForUse(productRequest):
    productCheck = productMaster.objects.values_list().filter(sku__iexact=productRequest).count()
    if productCheck==0:
        return True
    else:
        return False

def getProductIdFromSku(skuInUse):
    productCheck = productMaster.objects.values_list().filter(sku__iexact=skuInUse)
    return productCheck[0][0]

def isAllowedToPerformChangeOnProduct(productIdPassed):
    productCheck = productMaster.objects.values_list().filter(productId=productIdPassed).filter(isDeleted=0).count()
    if productCheck==1:
        return True
    else:
        return False

def isAllowedToPerformChangeOnProductDeletePerma(productIdPassed):
    productCheck = productMaster.objects.values_list().filter(productId=productIdPassed).filter(isDeleted=1).count()
    if productCheck==1:
        return True
    else:
        return False

def getTotalNotDeletedProductCount():
    productCheck = productMaster.objects.values_list().filter(isDeleted=0).count()
    return productCheck

def getTotalDeletedProductCount():
    productCheck = productMaster.objects.values_list().filter(isDeleted=1).count()
    return productCheck

def getTotalSearchProudctName(searchRequest):
    productCheck = productMaster.objects.values_list().filter(name__contains=searchRequest.searchKey).filter(isDeleted=0).count()
    print(productCheck)
    return productCheck

def getTotalSearchProudctSku(searchRequest):
    productCheck = productMaster.objects.values_list().filter(sku__contains=searchRequest.searchKey).filter(isDeleted=0).count()
    return productCheck

def getTotalSearchProudctDesc(searchRequest):
    productCheck = productMaster.objects.values_list().filter(desc__contains=searchRequest.searchKey).filter(isDeleted=0).count()
    return productCheck

def getTotalFilterCount(activeStatus):
    productCheck = productMaster.objects.values_list().filter(isActive=activeStatus).filter(isDeleted=0).count()
    return productCheck

def gettingProductListData(pageControlHolder):
    offset = pageControlHolder.pageCount*pageControlHolder.pageSize
    productCheck = productMaster.objects.values_list().filter(isDeleted=0).order_by('-productId')[offset:offset+pageControlHolder.pageSize]
    return productCheck

def gettingProductListDataDeleted(pageControlHolder):
    offset = pageControlHolder.pageCount*pageControlHolder.pageSize
    productCheck = productMaster.objects.values_list().filter(isDeleted=1).order_by('-productId')[offset:offset+pageControlHolder.pageSize]
    return productCheck

def gettingSearchResultForName(searchRequest, pageControlHolder):
    offset = pageControlHolder.pageCount*pageControlHolder.pageSize
    productCheck = productMaster.objects.values_list().filter(name__contains=searchRequest.searchKey).filter(isDeleted=0).order_by('-productId')[offset:offset+pageControlHolder.pageSize]
    return productCheck

def gettingSearchResultForSku(searchRequest, pageControlHolder):
    offset = pageControlHolder.pageCount*pageControlHolder.pageSize
    productCheck = productMaster.objects.values_list().filter(sku__contains=searchRequest.searchKey).filter(isDeleted=0).order_by('-productId')[offset:offset+pageControlHolder.pageSize]
    return productCheck

def gettingSearchResultForDesc(searchRequest, pageControlHolder):
    offset = pageControlHolder.pageCount*pageControlHolder.pageSize
    productCheck = productMaster.objects.values_list().filter(desc__contains=searchRequest.searchKey).filter(isDeleted=0).order_by('-productId')[offset:offset+pageControlHolder.pageSize]
    return productCheck

def gettingFilterValueResult(activeStatus, pageControlHolder):
    offset = pageControlHolder.pageCount*pageControlHolder.pageSize
    productCheck = productMaster.objects.values_list().filter(isActive=activeStatus).filter(isDeleted=0).order_by('-productId')[offset:offset+pageControlHolder.pageSize]
    return productCheck
