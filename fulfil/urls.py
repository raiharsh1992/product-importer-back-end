"""fulfil URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
import productImporter.views
urlpatterns = [
    #path('admin/', admin.site.urls),
    path('login/', productImporter.views.login),
    path('logout/', productImporter.views.logout),
    path('createproduct/', productImporter.views.createproduct),
    path('productstatus/', productImporter.views.productstatus),
    path('deleteproduct/', productImporter.views.deleteproduct),
    path('editproduct/', productImporter.views.editproduct),
    path('deleteproductperma/', productImporter.views.deleteproductperma),
    path('isuniquesku/', productImporter.views.isuniquesku),
    path('viewproducts/', productImporter.views.viewproducts),
    path('viewdelproducts/', productImporter.views.viewdelproducts),
    path('uploadproducts/', productImporter.views.uploadproducts),
    path('search/', productImporter.views.search),
    path('filter/', productImporter.views.filter),
    path('deleteall/', productImporter.views.deleteall),
    path('get_progress/', productImporter.views.get_progress),
    path('webhook/', productImporter.views.webhook),
]
