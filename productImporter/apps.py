from django.apps import AppConfig


class ProductimporterConfig(AppConfig):
    name = 'productImporter'
