import logging
import json
from datetime import datetime
from celery.decorators import task
from celery.utils.log import get_task_logger
import csv
import os
from .dataClasses import productHolder
from .dataViewHolder import getProductIdFromSku, isNewProductForUse
from .dataMaipulator import insertNewProductInformation, updateAllProductInfo
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from celery_progress.backend import ProgressRecorder
import requests
##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
######################UTILITY LAYER###########################
##############################################################
##############################################################
##############################################################

logger = get_task_logger(__name__)

def loggerHandling(location):
    logger1 = logging.getLogger(__name__)
    logger1.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
    file_handler = logging.FileHandler(location)
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger1.addHandler(file_handler)
    logger1.addHandler(stream_handler)
    return logger1

@task(name="Reading File and managing the insertion and updation process in the background", bind=True)
def uploadFileReadAndInsert(self, name, total_work_to_do):
    line_count = 0
    totalNewCreated = 0
    totalUpdated = 0
    progress_recorder = ProgressRecorder(self)
    for row in list(name):
        if line_count == 0:
            line_count += 1
        else:
            line_count += 1
            isActive = 1
            if line_count % 82 ==0:
                isActive = 0
            productInfo = productHolder(row[0], row[1], row[2], isActive, 0, None)
            if isNewProductForUse(productInfo.sku):
                print(row[0])
                if insertNewProductInformation(productInfo):
                    totalNewCreated +=1
            else:
                productInfo.productId = getProductIdFromSku(productInfo.sku)
                if updateAllProductInfo(productInfo):
                    totalUpdated +=1
            progress_recorder.set_progress(line_count, total_work_to_do)


@task(name="Sending Message to all active user about product(s) being added to the system")
def sendActiveUserNotificationProductNew(totalCount):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "listener", {"type": "user.listener",
        "event": "New Product",
        "productCount": totalCount}
    )

@task(name="Sending Message to all active user about product(s) being updated in the system")
def sendActiveUserNotificationProductUpdate(totalCount):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "listener", {"type": "user.listener",
        "event": "Product Updated",
        "productCount": totalCount}
    )

@task(name="Sending and hitting a webhook for the request")
def initiatedWebHookFromHere(sessionKey, userId, deviceIdentifier, totalNewCreated, totalUpdated):
    try:
        url = "http://192.168.0.2:8001/webhook/"
        headers = {"basicAuthenticate":sessionKey,'Content-Type': 'application/json'}
        payLoad = json.dumps({"userId":userId,"deviceIdentifier":deviceIdentifier,"newCreated":totalNewCreated,"totalUpdated":totalUpdated})
        response = requests.request("POST", url, headers=headers, data = payLoad)
        print(response.text.encode('utf8'))
    except Exception as e:
        logger.exception(e)

def handle_uploaded_file(fileNameUsed):
    total_work_to_do = 0
    csv_reader = bytearray()
    file = list()
    with open(fileNameUsed) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        file = list(csv_reader)
    if(total_work_to_do<=500001):
        os.remove(fileNameUsed)
        ab = uploadFileReadAndInsert.delay(file, len(file))
        return ab.task_id
    else:
        os.remove(fileNameUsed)
        return 0
