# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from .models import sessionInfo, productMaster
from .dataViewHolder import getLoginObject

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
###################DATA MANIPULATION LAYER####################
##############################################################
##############################################################
##############################################################

def insertSessionInfo(sessionInformation):
    try:
        sessionInsertion = sessionInfo()
        sessionInsertion.userId = getLoginObject(sessionInformation.userId)
        sessionInsertion.sessionKey = sessionInformation.sessionKey
        sessionInsertion.loginTime = sessionInformation.requestDateTime
        sessionInsertion.isActive = sessionInformation.isActive
        sessionInsertion.systemIdentifier = sessionInformation.deviceIdentifier
        sessionInsertion.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def performLogoutOption(sessionInformation):
    try:
        sessionActiveInfo = sessionInfo.objects.get(sessionKey=sessionInformation.sessionKey)
        sessionActiveInfo.isActive = 0
        sessionActiveInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False
def insertNewProductInformation(productRequest):
    try:
        productMasterInsert = productMaster()
        productMasterInsert.name = productRequest.name
        productMasterInsert.sku = productRequest.sku
        productMasterInsert.desc = productRequest.desc
        productMasterInsert.isActive = productRequest.isActive
        productMasterInsert.isDeleted = productRequest.isDeleted
        productMasterInsert.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def performStatusChangeOnProduct(productStatusManager):
    try:
        productActiveInfo = productMaster.objects.get(productId=productStatusManager.productId)
        productActiveInfo.isActive = productStatusManager.status
        productActiveInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def performDeletionOnProduct(productStatusManager):
    try:
        productActiveInfo = productMaster.objects.get(productId=productStatusManager.productId)
        productActiveInfo.isDeleted = productStatusManager.status
        productActiveInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def performDeletionOnProductPermanent(productStatusManager):
    try:
        productActiveInfo = productMaster.objects.get(productId=productStatusManager.productId)
        productActiveInfo.delete()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def updateAllProductInfo(productEditRequest):
    try:
        productActiveInfo = productMaster.objects.get(productId=productEditRequest.productId)
        productActiveInfo.name = productEditRequest.name
        productActiveInfo.sku = productEditRequest.sku
        productActiveInfo.desc = productEditRequest.desc
        productActiveInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def performAllSoftDeletion():
    try:
        productMaster.objects.update(isDeleted=1)
        return True
    except Exception as e:
        logger.exception(e)
        return False

def performAllHardDeletion():
    try:
        productMaster.objects.filter(isDeleted=1).delete()
        return True
    except Exception as e:
        logger.exception(e)
        return False
