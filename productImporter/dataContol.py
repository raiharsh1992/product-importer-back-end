# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import uuid
from datetime import datetime
from django.http import JsonResponse
from .dataViewHolder import getUserIdFromLoginInfo, validSessionInfo, isNewProductForUse, getProductIdFromSku, getTotalNotDeletedProductCount, gettingProductListData, gettingProductListDataDeleted, getTotalDeletedProductCount, getTotalSearchProudctName, getTotalSearchProudctSku, getTotalSearchProudctDesc, gettingSearchResultForDesc, gettingSearchResultForSku, gettingSearchResultForName, getTotalFilterCount, gettingFilterValueResult
from .dataClasses import sessionHolder

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA VIEWING HOLDER LAYER###################
##############################################################
##############################################################
##############################################################

def getLoginSessionInformation(loginRequest):
    userId = getUserIdFromLoginInfo(loginRequest)
    if userId==0:
        return userId
    else:
        basicKey = uuid.uuid4().hex
        createdOnDate =  datetime.now()
        sessionInformation = sessionHolder(userId, basicKey, loginRequest.deviceIdentifier, createdOnDate, 1)
        if validSessionInfo(sessionInformation)!=0:
            while validSessionInfo(sessionInformation)!=0:
                basicKey = uuid.uuid4().hex
                sessionInformation.sessionKey = basicKey
        return sessionInformation

def isAllowedSkuUpdate(productEditRequest):
    if isNewProductForUse(productEditRequest.sku):
        return True
    else:
        skuProductId = getProductIdFromSku(productEditRequest.sku)
        if skuProductId==productEditRequest.productId:
            return True
        else:
            return False

def generateProductNotDeletedPageView(pageControlHolder):
    totalProductCoutnActive = getTotalNotDeletedProductCount()
    pageControlHolder.pageCount = pageControlHolder.pageCount-1
    productListData = gettingProductListData(pageControlHolder)
    container = []
    for each in productListData:
        container.append({
            "productId":each[0],
            "name":each[1],
            "sku":each[2],
            "desc":each[3],
            "isActive":each[4]
        })
    response = JsonResponse({"Data":container,"totalProductCount":totalProductCoutnActive})
    response.status_code = 200
    return response

def generateProductDeletedPageView(pageControlHolder):
    totalProductCoutnActive = getTotalDeletedProductCount()
    pageControlHolder.pageCount = pageControlHolder.pageCount-1
    productListData = gettingProductListDataDeleted(pageControlHolder)
    container = []
    for each in productListData:
        container.append({
            "productId":each[0],
            "name":each[1],
            "sku":each[2],
            "desc":each[3],
            "isActive":each[4]
        })
    response = JsonResponse({"Data":container,"totalProductCount":totalProductCoutnActive})
    response.status_code = 200
    return response

def getSearchAndReturn(searchControlHolder, pageControlHolder):
    if searchControlHolder.searchIn == "NAME":
        totalProductCountSearchName = getTotalSearchProudctName(searchControlHolder)
        pageControlHolder.pageCount = pageControlHolder.pageCount-1
        productListData = gettingSearchResultForName(searchControlHolder, pageControlHolder)
        container = []
        for each in productListData:
            container.append({
                "productId":each[0],
                "name":each[1],
                "sku":each[2],
                "desc":each[3],
                "isActive":each[4]
            })
        response = JsonResponse({"Data":container,"totalProductCount":totalProductCountSearchName})
        response.status_code = 200
        return response
    elif searchControlHolder.searchIn == "SKU":
        totalProductCountSearchName = getTotalSearchProudctSku(searchControlHolder)
        pageControlHolder.pageCount = pageControlHolder.pageCount-1
        productListData = gettingSearchResultForSku(searchControlHolder, pageControlHolder)
        container = []
        for each in productListData:
            container.append({
                "productId":each[0],
                "name":each[1],
                "sku":each[2],
                "desc":each[3],
                "isActive":each[4]
            })
        response = JsonResponse({"Data":container,"totalProductCount":totalProductCountSearchName})
        response.status_code = 200
        return response
    elif searchControlHolder.searchIn == "DESC":
        totalProductCountSearchName = getTotalSearchProudctDesc(searchControlHolder)
        pageControlHolder.pageCount = pageControlHolder.pageCount-1
        productListData = gettingSearchResultForDesc(searchControlHolder, pageControlHolder)
        container = []
        for each in productListData:
            container.append({
                "productId":each[0],
                "name":each[1],
                "sku":each[2],
                "desc":each[3],
                "isActive":each[4]
            })
        response = JsonResponse({"Data":container,"totalProductCount":totalProductCountSearchName})
        response.status_code = 200
        return response
    else:
        response = JsonResponse({"Data":"Invalid search parameters passed"})
        response.status_code = 409
        return response

def filetingInProductAndReturnList(activeStatus, pageControlHolder):
    if activeStatus==1 or activeStatus==0:
        totalProductCountFilter = getTotalFilterCount(activeStatus)
        pageControlHolder.pageCount = pageControlHolder.pageCount-1
        productListData = gettingFilterValueResult(activeStatus, pageControlHolder)
        container = []
        for each in productListData:
            container.append({
                "productId":each[0],
                "name":each[1],
                "sku":each[2],
                "desc":each[3],
                "isActive":each[4]
            })
        response = JsonResponse({"Data":container,"totalProductCount":totalProductCountFilter})
        response.status_code = 200
        return response
    else:
        response = JsonResponse({"Data":"Invalid filter value passed"})
        response.status_code = 409
        return response
