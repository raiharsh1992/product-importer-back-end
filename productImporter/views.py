# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
import json
from django.http import JsonResponse
from .utilityControl import loggerHandling
from .workHolder import loginUser, logOutUser, creatingNewProduct, changingProductStatus, deletingExistingProduct, editingProductInfo, deletingExistingProductPermanently, checkingIfUniqueSku, viewingAllProductList, viewingAllDelProductList, handleFileUploadRequest, searchInProducts, filteringInProduct, deletingProductsAll, workingOnWebhookRequest
from celery.result import AsyncResult
logger = loggerHandling('log/requestViewHolder.log')

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
#################REQUEST VIEWING HOLDER LAYER#################
##############################################################
##############################################################
##############################################################

#Login Api Interaction Point Begins
def login(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Login in for'+str(request.body))
                response = loginUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Logout Api Interaction Point Begins
def logout(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Login out for'+str(request.body))
                response = logOutUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Creating new product Api Interaction Point Begins
def createproduct(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Creating new product for the request '+str(request.body))
                response = creatingNewProduct(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Change product status Api Interaction Point Begins
def productstatus(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Changing product status for the following request '+str(request.body))
                response = changingProductStatus(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Delete(soft) product Api Interaction Point Begins
def deleteproduct(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Deleting(soft) product for the following request '+str(request.body))
                response = deletingExistingProduct(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Edit product info Api Interaction Point Begins
def editproduct(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Editing product for the following request '+str(request.body))
                response = editingProductInfo(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Delete(hard/permanently) product Api Interaction Point Begins
def deleteproductperma(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Deleting(hard) product for the following request '+str(request.body))
                response = deletingExistingProductPermanently(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Checking if is unique product sku Api Interaction Point Begins
def isuniquesku(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking product sku for the following request '+str(request.body))
                response = checkingIfUniqueSku(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing all product list Api Interaction Point Begins
def viewproducts(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing product list for the following request '+str(request.body))
                response = viewingAllProductList(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing all deleted product list Api Interaction Point Begins
def viewdelproducts(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing deleted product list for the following request '+str(request.body))
                response = viewingAllDelProductList(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Uploading PRODUCT CSV file Api Interaction Point Begins
def uploadproducts(request):
    try:
        if(request.method=="POST"):
            response = handleFileUploadRequest(request)
            return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Performing search and returning product list Api Interaction Point Begins
def search(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Searching for a keystring in product list for the following request '+str(request.body))
                response = searchInProducts(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Performing filtering on basis of active status and returning product list Api Interaction Point Begins
def filter(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Searching for a keystring in product list for the following request '+str(request.body))
                response = filteringInProduct(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Delete all the products in both hard and soft form Api Interaction Point Begins
def deleteall(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Searching for a keystring in product list for the following request '+str(request.body))
                response = deletingProductsAll(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Delete all the products in both hard and soft form Api Interaction Point Begins
def webhook(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Webhook request for the following task '+str(request.body))
                response = workingOnWebhookRequest(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Checnking the progress of a celery task on the basis of taskId provided
def get_progress(request):
    rString = json.loads(request.body)
    result = AsyncResult(rString['task_id'])
    response_data = {
        'state': result.state,
        'details': result.info,
    }
    return JsonResponse({"Data":response_data})
