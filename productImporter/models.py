from django.db import models
from datetime import datetime
# Create your models here.
class loginMaster(models.Model):
    userId = models.AutoField(primary_key=True, max_length=32)
    userName = models.CharField(unique=True, blank=False, null=False, max_length=64)
    password = models.CharField(unique=False, blank=False, null=False, max_length=32)
    def __str__(self):
        return self.name

class sessionInfo(models.Model):
    sessionId = models.AutoField(primary_key=True, max_length=64)
    sessionKey = models.CharField(max_length=128, blank=False, unique=True, null=False)
    loginTime = models.DateTimeField(auto_now_add=True)
    isActive = models.BooleanField(default=1)
    systemIdentifier = models.CharField(max_length=255, blank=False, unique=False, null=False)
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    def __str__(self):
        return self.name

class productMaster(models.Model):
    productId = models.AutoField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255, unique=False, null=False)
    sku = models.CharField(max_length=255, unique=True, null=False)
    desc = models.TextField()
    isActive = models.BooleanField(default=1)
    isDeleted = models.BooleanField(default=0)
    def __str__(self):
        return self.name
