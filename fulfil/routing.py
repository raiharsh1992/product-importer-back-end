from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from productImporter.consumer import productConsumer

application = ProtocolTypeRouter({
    "websocket": URLRouter([
        path("notifications/", productConsumer),
    ])
})
