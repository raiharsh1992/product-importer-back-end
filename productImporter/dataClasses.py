# -*- coding: utf-8 -*-
##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
#################DATA CLASS AND OBJECT LAYER##################
##############################################################
##############################################################
##############################################################
class loginHolder:
    def __init__(self, userName, password, deviceIdentifier, userId):
        self.userName = userName
        self.password = password
        self.deviceIdentifier = deviceIdentifier
        self.userId = userId

class sessionHolder:
    def __init__(self, userId, sessionKey, deviceIdentifier, requestDateTime, isActive):
        self.userId = userId
        self.sessionKey = sessionKey
        self.deviceIdentifier = deviceIdentifier
        self.requestDateTime = requestDateTime
        self.isActive = isActive

class productHolder:
    def __init__(self, name, sku, desc, isActive, isDeleted, productId):
        self.name = name
        self.sku = sku
        self.desc = desc
        self.isActive = isActive
        self.isDeleted = isDeleted
        self.productId = productId

class productStatusHandler:
    def __init__(self, productId, status):
        self.productId = productId
        self.status = status

class pageControl:
    def __init__(self, pageCount, pageSize):
        self.pageCount = pageCount
        self.pageSize = pageSize

class searchControl:
    def __init__(self, searchKey, searchIn):
        self.searchKey = searchKey
        self.searchIn = searchIn
