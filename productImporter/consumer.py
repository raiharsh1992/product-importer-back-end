from channels.generic.websocket import AsyncJsonWebsocketConsumer


class productConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        await self.accept()
        await self.channel_layer.group_add("listener", self.channel_name)

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard("listener", self.channel_name)

    async def user_listener(self, event):
        await self.send_json(event)
